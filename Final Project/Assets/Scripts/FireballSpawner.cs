﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballSpawner : MonoBehaviour {

	public GameObject fireball;
	public float waitTime;
	public float minWait;
	public float maxWait;
	public bool stop;


	// Use this for initialization
	void Start () {
		StartCoroutine (spawnerWait());
	}
	
	// Update is called once per frame
	void Update () {
		waitTime = Random.Range (minWait, maxWait);
	}

	IEnumerator spawnerWait (){
		while (stop == false) {
			Instantiate (fireball);
			yield return new WaitForSeconds (waitTime);
		}
	}
}
