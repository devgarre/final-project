﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	//sets the tank's movement speed
	public int playerSpeed;
	public int jumpHeight;
	public int maxJumps;
	public int jumpsLeft;
	public Rigidbody2D rb;
	public float groundDistance;
	public bool flipped = false;
	public bool isGrounded;
	public GameObject paused;
	public GameObject currentGame;
	public LayerMask groundLayer;
	public AudioClip jumpSound;

	// Use this for initialization
	void Start () {
		Rigidbody2D rb = gameObject.GetComponent<Rigidbody2D> ();
	}

	// Update is called once per frame
	void Update () {
		
		//Draws Raycast in Scene window, but not in game window
		Debug.DrawRay(transform.position, Vector3.down * groundDistance, Color.green);
		//checks if player is grounded
		RaycastHit2D hit = Physics2D.Raycast (transform.position, Vector3.down, groundDistance, groundLayer);
		//resets player jumps when grounded
		if (hit.collider != null) {
			isGrounded = true;
			jumpsLeft = maxJumps;
		} 
		else {
			isGrounded = false;
		}

		//movement key inputs
		if (Input.GetKey ("left")) {
			transform.position -= transform.right * playerSpeed * Time.deltaTime;
			if (flipped == false) {
				transform.localScale = new Vector3(transform.localScale.x *-1, transform.localScale.y, transform.localScale.z);
				flipped = true;
			}
		}
		if (Input.GetKey ("right")) {
			transform.position += transform.right * playerSpeed * Time.deltaTime;
			if (flipped == true) {
				transform.localScale = new Vector3(transform.localScale.x *-1, transform.localScale.y, transform.localScale.z);
				flipped = false;
			}
		}
		if (Input.GetKeyDown ("up")) {
			//allows for set jumps while grounded or has jumps remaining
			if (jumpsLeft > 0) {
				jumpsLeft = jumpsLeft - 1;
				rb.velocity += jumpHeight * Vector2.up;
				AudioSource.PlayClipAtPoint (jumpSound, transform.position);
			} 
		}
		//opens pause menu
		if (Input.GetKeyDown ("escape")) {
			paused.SetActive (true);
			currentGame.SetActive (false);
		}
	}
}
