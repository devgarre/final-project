﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwapBack : MonoBehaviour {

	// Use this for initialization
	void Start () {
		SceneManager.LoadScene ("Final Project", LoadSceneMode.Single);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
