﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballDestroy : MonoBehaviour {

	public int speed;
	public int delay;

	// Use this for initialization
	void Start () {
		speed = Random.Range (200, 700);
		GetComponent<Rigidbody2D>().AddRelativeForce(Random.onUnitSphere * speed);
		Destroy (gameObject, delay);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
