﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointSwap : MonoBehaviour {

	public Transform location;
	public GameObject flag;
	public AudioClip ding;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other){
		if (other.tag == "Player") {
			AudioSource.PlayClipAtPoint (ding, transform.position);
			Instantiate (flag, location.position, location.rotation);
		}
	}
}
